#!/usr/bin/env bash
echo -en "Checking for sufficient permissions... "
for bin in docker-compose docker git; do
  if [[ -z $(which ${bin}) ]]; then echo "Cannot find ${bin}, exiting..."; exit 1; fi
done

read -p "Are you sure you want to update? [y/N] " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then
    echo "Updating Water-Level-Monitor"
    echo "Updating backend repository"  
    cd ../water-level-monitor-backend
    git pull
    ./gradlew build -x test
    cd ../water-level-monitor-dockerized

    echo "Stopping containers" 
    docker-compose down

    echo "Building new containers" 
    docker-compose up --build -d
	echo "Finished"
    exit 0
else
  echo "bye!"
  exit 0
fi
